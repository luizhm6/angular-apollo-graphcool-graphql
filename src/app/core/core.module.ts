import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';


import { ApolloConfigModule } from './../apollo-config.module';

@NgModule({
	exports: [
		BrowserModule,
		BrowserAnimationsModule,
		ApolloConfigModule,
		MatButtonModule,
		MatCheckboxModule
	]
})
export class CoreModule { }
