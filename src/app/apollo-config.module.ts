import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { ApolloModule, Apollo } from 'apollo-angular';
import { onError } from 'apollo-link-error';
import { HttpLinkModule, HttpLink } from 'apollo-angular-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink } from 'apollo-link';

@NgModule({
	declarations: [],
	imports: [ 
		HttpClientModule,
		ApolloModule,
		HttpLinkModule,
		CommonModule 
	],
	exports: [],
	providers: [],
})

export class ApolloConfigModule {
	constructor(
		private apollo: Apollo,
		private httpLink: HttpLink
	){
		const uri =  'https://api.graph.cool/simple/v1/cjkvwm9dp0ozg013951sorp8s';
		const linkError = onError(({ graphQLErrors, networkError }) => {
			if (graphQLErrors)
			  graphQLErrors.map(({ message, locations, path }) =>
				console.log(
				  `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`,
				),
			  );
		  
			if (networkError) console.log(`[Network error]: ${networkError}`);
		  });

		const http = httpLink.create({ uri });
		apollo.create({
			link: ApolloLink.from([
				linkError,
				http
			]),
			cache: new InMemoryCache()
		})
	}
}