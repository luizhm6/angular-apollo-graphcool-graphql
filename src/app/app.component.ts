import { Component } from '@angular/core';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag'
@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent {

	constructor(
		private apollo: Apollo
	) {
		this.createUsser()
	}

	allUsers(): void {

		this.apollo.query({
			query: gql`
				query{
					allUsers {
						id
						name
						email
					}
				}
			`
		}).subscribe(res => console.log(res))
	}
	createUsser(): void {
		this.apollo.mutate({
			mutation: gql`
				mutation CreateNewUser($name: String!, $email: String!, $password: String!) {
					createUser(name: $name, email: $email, password: $password) {
						id
						name
						email
					}
				}
			`, variables: {
				name: 'Sion',
				email: 'sion.undead@mail.com',
				password: '123456'
			}
		}).subscribe(res => {
			console.log(res)
			this.allUsers()
		})

	}
}
